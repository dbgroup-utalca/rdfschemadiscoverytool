
import com.hp.hpl.jena.tdb.store.Hash;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author mauricio
 */
public class Structuredness {

    private RDFSchema schema;
    private Statistics statistics;
    private QueryExecutor queryExecutor;
    
    public Structuredness(RDFSchema schema, QueryExecutor queryExecutor) {
        this.schema = schema;
        this.statistics = new Statistics(schema);
        this.queryExecutor = queryExecutor;
    }

    public float evaluate() {
        ArrayList<String> resources = this.queryExecutor.getResources();
        ArrayList<String> properties;
        
        for (String resource : resources) {
            properties = this.queryExecutor.getProperties(resource);
            Type type = classify(properties);
            this.statistics.addInstance(type);
            for (String s : properties) {
                this.statistics.addOcurrence(type, s);
            }
        }
        
        HashMap<Type, Float> coverage = this.statistics.getCoverage();
        Set<Type> keySet = coverage.keySet();
        for (Type type : keySet) {
            System.out.println("type: " + type.getName() + " value: " + coverage.get(type));
        }
        
        this.statistics.printInstances();
        this.statistics.printOcurrences();
        return -1;
    }

    /**
     * 
     * @param properties Conjunto de propiedades de un recurso
     * @return El tipo al que corresponde el recurso
     */
    private Type classify(ArrayList<String> properties) {
        Type type = null;
        int coincidences = 0;
        
        for (Type t : this.schema.getTypes()) {
            int counter = 0;
            for (String prop : properties) {
                if (t.containsProperty(prop))
                    counter++;
            }
            if (counter >= coincidences) {
                coincidences = counter;
                type = t;
            }
        }
        return type;
    }
    
    
    
    
}
