
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Iterator;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author mauricio
 */
class SchemaExtractor {
    
    public final static int JENA_RAM = 0;
    public final static int JENA_TDB = 1;
    
    private String filename;
    private QueryExecutor queryExecutor;
    private float mergeValue;
    
    /**
     * 
     * @param filename of the dataset
     * @param mergeValue parameter to merge characteristic sets
     * @param dbPercent percentage of resources to explore
     */
    public SchemaExtractor(QueryExecutor queryExecutor, float mergeValue){
        this.queryExecutor = queryExecutor;
        this.mergeValue = mergeValue;
    }

    /**
     * 
     * @param dbPercent Porcentaje de recursos a explorar
     * @return 
     */
    private RDFSchema extract(float dbPercent) {
        RDFSchema schema = new RDFSchema();
        int counter = 0;
        
        // get all resources
        ArrayList<String> resources = this.queryExecutor.getResources();
        int totalResources = resources.size();
        
        System.out.println("[+] Total recursos: " + totalResources);
                        
        // remove resources
        Iterator<String> iterator = resources.iterator();
        int i = 0;
        while (iterator.hasNext()) {
            String resource = iterator.next();
            if (i >= dbPercent * 100) {
                iterator.remove();
            }
            if (i == 99) {
                i = -1;
            }
            i++;
        }
                
        System.out.println("[+] Porción de recursos a considerar: " + resources.size());
        
        int num = 0;
        for (String resource : resources){
            if (num % 10000 == 0 && num != 0) {
                System.out.println("num: " + num);
            }
            num++;
            
            ArrayList<String> properties = this.queryExecutor.getProperties(resource);
            
            if (properties.size() > 0) {
                if (properties.contains("http://www.w3.org/1999/02/22-rdf-syntax-ns#type")){ // new characteristic set with specific label
                    String objectValue = this.queryExecutor.getObject(resource, "http://www.w3.org/1999/02/22-rdf-syntax-ns#type");
                    Type cSet = new Type(objectValue, properties);
                    schema.add(cSet);
                }
                else {  // new characteristic set with generic label
                    Type cSet = new Type("CS"+counter, properties);
                    schema.add(cSet);
                    counter++;
                }
                
            }
        }
        
        System.out.println("[+] CS generados\n");
        return schema;
    }
    
    public RDFSchema merge(RDFSchema schema){
        System.out.println("[+] Mezclando...");
        int size = schema.typesNumber();
        RDFSchema merged = new RDFSchema();
        BitSet bitSet = new BitSet(size);
        int iName = 0;
        
        for (int i = 0; i < size; i++) {
            for (int j = 1; j < size; j++) {
                Type cSet1 = schema.get(i);
                Type cSet2 = schema.get(j);
                if ( (i != j) && !bitSet.get(i) && !bitSet.get(j) && cSet1.similarity(cSet2) >= mergeValue){     
                    // both sets with generic name
                    if (cSet1.getName().startsWith("CS") && cSet2.getName().startsWith("CS")){
                        merged.add(new Type("CS"+iName).addProperties(cSet1.getProperties())
                            .addProperties(cSet2.getProperties()));
                        iName++;
                        bitSet.set(i);
                        bitSet.set(j);
                    }
                    // at least one characteristic set with real name
                    else if (!cSet1.getName().startsWith("CS") || !cSet2.getName().startsWith("CS")){
                        String name;
                        if (!cSet1.getName().startsWith("CS") && cSet2.getName().startsWith("CS")){ // :1 real y :2 generic
                            name = cSet1.getName();
                            merged.add(new Type(name).addProperties(cSet1.getProperties())
                                .addProperties(cSet2.getProperties()));
                            bitSet.set(i);
                            bitSet.set(j);
                        }
                        else if (cSet1.getName().startsWith("CS") && !cSet2.getName().startsWith("CS")){ // :1 generic y :2 real
                            name = cSet2.getName();
                                merged.add(new Type(name).addProperties(cSet1.getProperties())
                                .addProperties(cSet2.getProperties()));
                            bitSet.set(i);
                            bitSet.set(j);
                        }
                        else { // both with real name
                            if (cSet1.getName().equals(cSet2.getName())){  // sets with same name
                                merged.add(new Type(cSet1.getName()).addProperties(cSet1.getProperties())
                                    .addProperties(cSet2.getProperties()));
                                bitSet.set(i);
                                bitSet.set(j);
                            }
                        }
                        
                    }
                }
            }
        }
        

        // include unmerge characteristic sets
        for (int i = 0; i < size; i++) {
            if (!bitSet.get(i) && schema.get(i).getProperties().size() > 0){
                if (schema.get(i).getName().startsWith("CS")){
                    merged.add(schema.get(i).setName("CS" + iName));
                    iName++;
                }
                else {
                    merged.add(schema.get(i));
                }
            }
        }
        
        return merged;
    }
    
    
    public RDFSchema getSchema(float dbPercent){
        // get basic CSs from each distinct resource
        RDFSchema basicSchema = this.extract(dbPercent);
        
//        basicSchema.unsort();
        
        // merging once
        RDFSchema merged = this.merge(basicSchema);
        RDFSchema aux;

        // merging several times to generate a reduced schema
        while (!(aux = this.merge(merged)).equals(merged)){
            merged = aux;
        }
        
        // return schema
        return merged;
    }
    
}
