
import java.util.HashMap;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author mauricio
 */
public class Statistics {
    
    private RDFSchema schema;
    private HashMap<Type, Integer> instances;
    private HashMap<Type, HashMap<String, Integer>> ocurrences;
    
    public Statistics (RDFSchema schema) {
        this.schema = schema;
        
        this.instances = new HashMap<>();
        for (Type type : this.schema.getTypes())
            this.instances.put(type, 0);
        
        this.ocurrences = new HashMap<>();
        for (Type type : this.schema.getTypes()) {
            HashMap<String, Integer> map = new HashMap<>();
            for (String property : type.getProperties()) {
                map.put(property, 0);
                this.ocurrences.put(type, map);
            }
        }
            
    }
    
    public void addInstance (Type type) {
        Integer counter = this.instances.get(type);
        this.instances.put(type, counter+1);
    }
    
    public void addOcurrence (Type type, String property) {
        HashMap<String, Integer> map = this.ocurrences.get(type);
        Integer counter = map.get(property);
        map.put(property, counter+1);
    }
    
    public void printInstances () {
        for (Type type : this.schema.getTypes()) {
            System.out.println(type.getName() + " - " + this.instances.get(type));
        }
    }
    
    public void printOcurrences () {
        for (Type type : this.schema.getTypes()) {
            for (String prop : type.getProperties()) {
                HashMap<String, Integer> map = this.ocurrences.get(type);
                Integer get = map.get(prop);
                System.out.println("tipo: " + type.getName() + " propiedad: " + prop + " cant: " + get);
            }
        }
    }

    public HashMap<Type, Float> getCoverage() {
        HashMap<Type, Float> coverage = new HashMap<>();
        
        for (Type type : this.schema.getTypes()) {
            Integer sum = 0;
            HashMap<String, Integer> map = this.ocurrences.get(type);
            for (String prop : type.getProperties()) {
                sum += map.get(prop);
            }
            int denominador = type.getPropertiesSize() * this.instances.get(type);
            
            float value = (float) (sum / (denominador*1.0));
            coverage.put(type, value);
        }
        
        return coverage;
    }
    
}
