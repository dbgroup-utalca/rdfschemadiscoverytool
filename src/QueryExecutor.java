
import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QueryParseException;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.tdb.TDBFactory;
import java.util.ArrayList;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author mauricio
 */
class QueryExecutor {
 
    public static final int JENA_RAM = 0;
    public static final int JENA_TDB = 1;
    
    private Model model = null; // in memory
    private ArrayList<String> resources = null;
    
    public QueryExecutor(String filename, int jenaType){
        switch (jenaType) {
            case SchemaExtractor.JENA_RAM:
                this.model = RDFDataMgr.loadModel(filename);
                break;
            case SchemaExtractor.JENA_TDB:
                Dataset dataset = TDBFactory.createDataset(filename);
                this.model = dataset.getDefaultModel();
                dataset.end();
                break;
            default:
                System.out.println("Error constructor QueryExecutor");
                System.exit(-1);
        }
    }
    
    public ArrayList<String> getResources(){
        if (this.resources != null)
            return this.resources;
        
        this.resources = new ArrayList<>();
        
        String queryString = "SELECT DISTINCT ?x " + 
                             "WHERE { ?x ?y ?z }";
        
        Query query = QueryFactory.create(queryString);
        QueryExecution qe = QueryExecutionFactory.create(query, this.model);
        
        ResultSet results = qe.execSelect();
        
        while (results.hasNext()){
            QuerySolution solution = results.next();
            this.resources.add(solution.getResource("?x").toString());
        }
        
        return this.resources;
    }

    ArrayList<String> getProperties(String resource) {
        ArrayList<String> properties = new ArrayList<>();
        
        String queryString = "SELECT DISTINCT ?y " +
                             "WHERE { <" + resource + "> ?y ?z }";
        
        try {
            Query query = QueryFactory.create(queryString);
            QueryExecution qe = QueryExecutionFactory.create(query, this.model);

            ResultSet results = qe.execSelect();

            while (results.hasNext()){
                QuerySolution next = results.next();
                properties.add(next.get("?y").toString());
            }
        } catch (QueryParseException e) {
            System.out.println("[-] Error obteniendo propiedades de recurso: " + resource);
        }
        
        return properties;
    }
    
    public String getObject(String resource, String property){
        String queryString = "SELECT distinct ?z "+
                             "WHERE { <" + resource + "> <" + property + "> ?z } limit 1";
        
        Query query = QueryFactory.create(queryString);
        QueryExecution qe = QueryExecutionFactory.create(query, this.model);
        
        ResultSet results = qe.execSelect();
        String value = results.next().get("?z").toString();
        
//        if (value.contains("#"))
//            return value.substring(0, value.indexOf("#"));
        return value;
    } 
}
