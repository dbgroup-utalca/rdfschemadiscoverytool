
import java.util.ArrayList;
import java.util.Collections;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author mauricio
 */
public class RDFSchema {
 
    private ArrayList<Type> types;
    
    public RDFSchema () {
        this.types = new ArrayList<>();
    }
    
    public void add (Type type) {
        this.types.add(type);
    }

    public int typesNumber () {
        return this.types.size();
    }

    public Type get(int i) {
        return this.types.get(i);
    }

    public ArrayList<Type> getTypes() {
        return this.types;
    }
    
    @Override
    public boolean equals (Object o) {
        RDFSchema aux = (RDFSchema) o;
        return this.types.equals(aux.getTypes());
    }

    public void unsort() {
        Collections.shuffle(this.types);
    }
}
