
import java.util.ArrayList;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author mauricio
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        String filename = "/home/mauricio/Descargas/dataset/latest_reegle_dump.nt";
        //String filename = "/home/morellana/datasets/geospecies.rdf";
        float dbPercent = (float) 1;
        float mergeValue = (float) 0.5;
        
        // crear objeto encargado de manejar las consultas al modelo
        QueryExecutor queryExecutor = new QueryExecutor(filename, QueryExecutor.JENA_RAM);
        
        SchemaExtractor extractor = new SchemaExtractor(queryExecutor, mergeValue);
        
        long iTime = System.currentTimeMillis();
        RDFSchema schema = extractor.getSchema(dbPercent);
        long eTime = System.currentTimeMillis();
        
        print(schema);
        
        // Statistics
        System.out.println("[+] #CS: " + schema.typesNumber());
        System.out.println("[+] Tiempo: " + (eTime - iTime) + " ms.");
        
        Structuredness structuredness = new Structuredness(schema, queryExecutor);
//        structuredness.evaluate();
//        Statistics st = new Statistics(schema);
//        st.printCoincidences();
//        st.printOcurrences();
    }

    
    private static void print(RDFSchema results) {
        float totalProperties = 0;
        for (Type set : results.getTypes()){
            System.out.println(set.getName());
            totalProperties += set.getPropertiesSize();
            
            for (String prop : set.getProperties()){
                System.out.println("\t"+prop);
            }
        }
        System.out.println("[+] Propiedades promedio: " + totalProperties/results.typesNumber());
    }
    
}
