# **Herramienta para el descubrimiento de esquema RDF** #

## **Descripción** ##

Esta herramienta permite descubrir el esquema de un dataset RDF en el peor caso, es decir, cuando éste no contiene información asociada a su modelo de datos (rdf:type, rdf:Property, rdfs:range, etc)